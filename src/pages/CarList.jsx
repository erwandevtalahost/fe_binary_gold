import { useState,useEffect } from "react"
import {useNavigate } from "react-router-dom";
const URL = "https://bootcamp-rent-car.herokuapp.com/admin/car";

import Skeleton from '@mui/material/Skeleton';
import Stack from '@mui/material/Stack';



export default function CarList() {
    const [cars,setCars] = useState([])
    const [loading, setLoading] = useState(false)

    let navigate = useNavigate()

    async function getCars() {
        try{
            setLoading(true)
            const res = await window.fetch(URL);
            const data = await res.json();
            console.log(data)
            setCars(data)
            setLoading(false)
        }catch(e) {
            setLoading(false)
            console.log(e)
        }

    }

    useEffect(() => {
        getCars()
    },[])

    function handleViewDetail(id) {
        navigate(`/mobildetail/${id}`)
    }


    function handleFilterChange() {
        const inputName = 'Camry'
        const capacity = '2 -4 orang';
        const price =  500000

        if(cars.length > 0){
            
            const filterData = cars.filter(item =>
             item.category === capacity
             || item.price  === price 
             || item.name === inputName    
            );
            setCars(filterData)
        }
    }

    return (
        <>
        <h1>Halaman List Mobil</h1>
        <ul>
            {loading ? (
    <Stack spacing={1}>
      <Skeleton variant="text" />
      <Skeleton variant="circular" width={40} height={40} />
      <Skeleton variant="rectangular" width={210} height={118} />
    </Stack>
  ) : cars.map(car => (
                <li key={car.id}>{car.image ? (
                    <img src={car.image} alt={car.name}/> 
                    ) : (
                    <div>No Image</div>
                    )}
                  
                    <div>{car.name}</div>
                    <div>{car.price}</div>
                    <button onClick={() => handleViewDetail(car.id)}>View Detail</button>
                </li>
            ))}
        </ul>
        </>
    )
}