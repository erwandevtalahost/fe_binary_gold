import { Container, Typography } from '@mui/material';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import FormControl from '@mui/material/FormControl';
import Grid from '@mui/material/Grid';
import { borderColor, Box } from '@mui/system';
import { useState, useEffect } from "react"

import InputLabel from '@mui/material/InputLabel';

import NativeSelect from '@mui/material/NativeSelect';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import NumberFormat from 'react-number-format';
import Stack from '@mui/material/Stack';
import Skeleton from '@mui/material/Skeleton';
import TextField from '@mui/material/TextField';
import { useNavigate } from "react-router-dom";
const URL = "https://bootcamp-rent-car.herokuapp.com/admin/car";




export default function CariMobil() {

    const [w, setW] = useState(window.innerHeight > 734 ? 50 : 70);

    useEffect(() => {
        const handleResize = () => {
            setW(window.innerHeight > 734 ? 50 : 70);
        };

        window.addEventListener("resize", handleResize);

        return () => {
            window.removeEventListener("resize", handleResize);
        };
    }, []);

    //=---------
    const [loading, setLoading] = useState(false)
    const [cars, setCars] = useState([])

    let navigate = useNavigate()

    async function getCars() {
        try {
            setLoading(true)
            const res = await window.fetch(URL);
            const data = await res.json();
            console.log(data)
            setCars(data)
            setLoading(false)
        } catch (e) {
            setLoading(false)
            console.log(e)
        }

    }

    useEffect(() => {
        getCars()
    }, [])

    function handleViewDetail(id) {
        navigate(`/detailmobil/${id}`)
    }




    return (
        <>
            <main>
                <Box mt={w} sx={{ flexGrow: 1 }}>
                    <Container maxWidth="lg" sx={{
                        overflow: 'auto'
                    }}>
                        <Box
                            sx={{

                                maxwidth: "100%",
                                height: "100px",
                                padding: 3,
                                margin: 0,
                                // border: '1px solid black' ,
                                align: "center"

                            }}>

                            <Stack direction="row" spacing={2} sx={{
                                boxShadow: "12px"
                            }}>
                                
                                <TextField

                                    id="outlined-required"
                                    label="Nama Mobil"
                                    defaultValue="Ketik nama/tipe mobil"
                                    sx={{
                                        width: "800px"
                                    }}
                                />


                                <FormControl fullWidth>
                                    <InputLabel id="demo-simple-select-label">Kategori</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        label="Kategori"
                                        placeholder="Apa saja syarat yang dibutuhkan"

                                    >
                                        <MenuItem value={10}>Masukan Kapasitas Mobil</MenuItem>

                                    </Select>


                                </FormControl>

                                <FormControl fullWidth >
                                    <InputLabel id="demo-simple-select-label">Harga</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        label="Kategori"
                                        placeholder="Apa saja syarat yang dibutuhkan"
                                    >

                                        <MenuItem value={10}>Masukan Harga Sewa per Hari</MenuItem>
                                    </Select>


                                </FormControl>

                                <FormControl fullWidth>
                                    <InputLabel id="demo-simple-select-label">Status</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        label="Kategori"
                                        placeholder="Apa saja syarat yang dibutuhkan"
                                    >

                                        <MenuItem value={10}>Disewa</MenuItem>
                                    </Select>


                                </FormControl>
                                <Button variant='contained' color='success' sx={{
                                    fontSize: "10px"
                                }}>Edit</Button>


                            </Stack>
                        </Box>

                        <Box m={10}

                        >
                            <Grid container spacing={4}>
                                {loading ? (
                                    <Stack spacing={1}>
                                        <Skeleton variant="text" />
                                        <Skeleton variant="circular" width={40} height={40} />
                                        <Skeleton variant="rectangular" width={210} height={118} />
                                    </Stack>
                                ) : cars.map((itemmobile) => (

                                    <Grid item key={itemmobile} xs={12} sm={6} md={4}>
                                        <Card
                                            sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}
                                        >
                                            <CardMedia
                                                component="img"
                                                sx={{
                                                    // 16:9
                                                    pt: '16.25%',
                                                }}
                                                image={itemmobile.image}
                                                alt="random"
                                            />
                                            <CardContent sx={{ flexGrow: 1 }}>
                                                <Typography gutterBottom variant="h5" component="h5" sx={{
                                                    fontSize: "14px"
                                                }}>
                                                    {itemmobile.name}
                                                </Typography>
                                                <Typography gutterBottom variant="h5" component="h5" sx={{
                                                    fontSize: "16px",
                                                    fontWeight: "bold"
                                                }}>
                                                    <NumberFormat
                                                        value={itemmobile.price}
                                                        className="foo"
                                                        displayType={'text'}
                                                        thousandSeparator={true}
                                                        prefix={'Rp '}

                                                    />
                                                </Typography>
                                                <Typography>
                                                    This is a media card. You can use this section to describe the
                                                    content.
                                                </Typography>
                                            </CardContent>
                                            <CardActions sx={{

                                                justifyContent: "center",

                                            }}>
                                                <Button sx={{
                                                    width: "300px"
                                                }} size="large" variant='contained' color='success' onClick={() => handleViewDetail(itemmobile.id)}> Pilih Mobil</Button>

                                            </CardActions>
                                        </Card>
                                    </Grid>

                                ))}

                            </Grid>

                        </Box>


                    </Container>

                </Box>
            </main>
        </>
    )
}
