
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import Paper from '@mui/material/Paper';

import Toolbar from '@mui/material/Toolbar';
import { useNavigate } from "react-router-dom";
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

import { useRef } from "react";
import { Grow } from '@mui/material';

import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';
import './styles.css';
import { Link } from "react-router-dom";
import Stack from '@mui/material/Stack';
import { useState, useEffect } from "react"
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';

import gambar from '../assets/img_car.png';
import Footer from './Footer';
const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));



const drawerWidth = 240;

const navItems = ['Our Services', 'Why Us', 'Testimonial', 'FAQ'];
const menu_refs = ['services','why_us','testimoni','faq'];
export default function Header() {

    // const { window } = props;
    const [mobileOpen, setMobileOpen] = useState(false);
    let navigate = useNavigate()

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    function sewaMobil() {
        navigate(`/pencariandata`);
    }

    const services = useRef(null);
    const why_us = useRef(null);
    const testimonial = useRef(null);
    const faq = useRef(null);

    const scrollToSection = (elementRef) => {
        window.scrollTo({
            top: elementRef.current.offsetTop,
            behavior: "smooth",
        });
    };

    const drawer = (
        <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
            <Typography variant="h6" sx={{ my: 2 }}>
                MUI
            </Typography>

            <List>
                {navItems.map((item) => (
                    <ListItem key={item} disablePadding>
                        <ListItemButton sx={{ textAlign: 'center' }}>
                            <ListItemText primary={item} />
                        </ListItemButton>
                    </ListItem>
                ))}
            </List>
        </Box>
    );

    return (
        <>
            <Box
                sx={{ flexGrow: 1 }}
            >
                <AppBar position="absolute" sx={{
                    background: "#F1F3FF"
                }}>
                    <Container>
                        <Toolbar variant="dense" sx={{
                            background: "#F1F3FF"
                        }}>
                            <IconButton
                                color="inherit"
                                aria-label="open drawer"
                                edge="start"
                                onClick={handleDrawerToggle}
                                sx={{ mr: 2, display: { sm: 'none' } }}
                            >
                                <MenuIcon />
                            </IconButton>
                            <Typography
                                variant="h6"
                                component="div"
                                sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' }, color: '#000000' }}
                            >

                                <Box sx={{
                                    color: "#0D28A6",
                                    background: "#0D28A6",
                                    width: "100px",
                                    height: "34px",
                                    marginTop: "10px",
                                    marginLeft: "-30px"
                                }}>

                                </Box>


                            </Typography>
                            <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
                                {navItems.map((item) => (
                                    <Button key={item} sx={{ color: '#000000' }}>
                                        {item}
                                    </Button>
                                ))}
                            </Box>
                        </Toolbar>
                        <Grid container spacing={2} columns={{ xs: 4, sm: 6, md: 12 }}>

                            <Grid item xs={6} sm={4} md={6}>
                                <Box
                                    sx={{

                                        height: 100,

                                    }}
                                />
                                <Typography fontSize={36} fontWeight={700} sx={{
                                    color: '#000000',
                                    fontFamily: "Arial"
                                }}>
                                    <strong>Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</strong>
                                </Typography>
                                <Typography fontSize={14} fontFamily={"Arial"} fontWeight={700} sx={{
                                    color: '#000000'
                                }}>
                                    selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik
                                    dengan harga terjangkau. Selalu siap melayani kebutuhanmu
                                    untuk sewa mobil selama 24 jam.
                                </Typography>
                               <Button variant='contained' color="success" onClick={() => sewaMobil()} sx={{ marginTop: "10px" }}>Mulai Sewa Mobil</Button>
                            </Grid>
                            <Grid item xs={6} sm={6} md={6}>
                                <Box sx={{
                                    maxwidth: "100%",
                                    height: "auto",
                                    padding: 0,
                                    margin: 0
                                }}>
                                    <img src={gambar} />
                                </Box>

                            </Grid>
                        </Grid>

                    </Container>
                </AppBar>




            </Box>

        </>
    );
}
