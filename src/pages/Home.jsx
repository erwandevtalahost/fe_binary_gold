import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

import Container from '@mui/material/Container';

import Grid from '@mui/material/Grid';
import foto from '../assets/img_service.png';
import foto2 from '../assets/icon_complete.png';
import profilorang from '../assets/img_photo.png';
import bintang from '../assets/bintang.jpeg';
import { styled } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { Button, Stack } from '@mui/material';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Paper from '@mui/material/Paper';
import Rating from '@mui/material/Rating';
import { useNavigate } from "react-router-dom";

import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { Link } from "react-router-dom";
import { green } from '@mui/material/colors';
import Icon from '@mui/material/Icon';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useState, useEffect } from "react"
import Carousel from 'react-material-ui-carousel'




const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

const item_data = [
  {
    name: "John Dee 32, Bromo",
    description: "“Lore Ipsum testing consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, , consectetur adipiscing elit, sed do eiusmod”",

  },
  {
    name: "John Dee 32, Bromo",
    description: "“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”"
  }
]


const faq = [
  {
    id: "1",
    faqnya: "Apa saja syarat yang dibutuhkan"
  },

  {
    id: "2",
    faqnya: "Berapa hari minimal sewa mobil lepas kunci?"
  },

  {
    id: "3",
    faqnya: "Berapa hari sebelumnya sabaiknya booking sewa mobil?"
  },

  {
    id: "4",
    faqnya: "Apakah Ada biaya antar-jemput?"
  },

  {
    id: "5",
    faqnya: "Bagaimana jika terjadi kecelakaan"
  }


]



const bull = (
  <Box
    component="span"
    sx={{ display: 'inline-block', mx: '2px', transform: 'scale(0.8)' }}
  >
    •
  </Box>
);


export default function Home() {
  const [w, setW] = useState(window.innerHeight > 734 ? 70 : 50);
  let navigate = useNavigate()

  function sewaMobil() {
    navigate(`/pencariandata`);
  }

  useEffect(() => {
    const handleResize = () => {
      setW(window.innerHeight > 734 ? 70 : 50);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);




  return (
    <>
      <Box mt={w} sx={{ flexGrow: 1 }}>
        <Container sx={{
          overflow: 'auto'
        }}>
          <Grid container spacing={2} columns={{ xs: 4, sm: 8, md: 12 }} >
            <Grid item xs={6} sm={8} md={6}>
              <Box sx={{
                maxwidth: "100%",
                height: "auto",
                padding: 0,
                margin: 0
              }}>
                <img src={foto} alt="tetststt" />


              </Box>






            </Grid>
            <Grid item xs={6} sm={8} md={6}>
              {/* <Box> */}
              <Typography

                variant='subtitle1'
                align='justify'
                component="div"
                gutterBottom

                sx={{
                  textAlign: "left",
                  fontWeight: "bold"

                }}
              >
                <h1>Best Car Rental for any kind of trip in (Lokasimu)!</h1>


                <h3>Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru,
                  serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</h3>


              </Typography>
              <Typography variant='subtitle1' sx={{
                textAlign: "left",
                fontSize: "18px",
                fontWeight: "bold"
              }}>
                <ul>
                  <li>Sewa Mobil Dengan Supir di Bali</li>
                  <li>Sewa Mobil Lepas Kunci di Bali 24 Jam</li>
                  <li>Sewa Mobil Jangka Panjang Bulanan</li>
                  <li>Gratis Antar - Jemput Mobil di Bandara</li>
                  <li>Layanan Airport Transfer / Drop In Out</li>
                </ul>
              </Typography>


              {/* </Box> */}

            </Grid>
          </Grid>
          <Box sx={{
            marginTop: "20px",


          }}>

            <Typography sx={{
              fontSize: "26px",
              fontWeight: "bold",
              fontFamily: "Arial",
              lineHeight: "60px"
            }}>
              Why Us ?

            </Typography>
            <Typography sx={{
              fontSize: "16px",
              fontWeight: "bold",
              fontFamily: "Arial",
              lineHeight: "20px"
            }}>
              Mengapa harus pilih binar Car Rental

            </Typography>


          </Box>
          <Box sx={{
            marginTop: "20px",
          }}>
            <Container fixed={true} sx={{
              align: "center"
            }}>
              <Grid container spacing={5} columns={{ xs: 4, sm: 6, md: 12 }}>
                <Grid item xs={3} sm={3} md={3}>
                  <Card sx={{ minWidth: 250, borderColor: 'black', fontWeight: "bold", fontFamily: "Arial" }}>
                    <CardContent>
                      <Typography sx={{ fontSize: 14, lineHeight: "-80px" }} color="text.secondary">
                        <img src={foto2} />
                      </Typography>
                      <Typography sx={{ fontSize: "16px", lineHeight: "50px", fontWeight: "bold", fontFamily: "Arial" }} >
                        Mobil Lengkap
                      </Typography>
                      <Typography sx={{ fontSize: "14px", lineHeight: "24px", fontWeight: "bold" }} >
                        Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat
                      </Typography>

                    </CardContent>

                  </Card>
                </Grid>


                <Grid item xs={3} sm={3} md={3}>
                  <Card sx={{ minWidth: 250, borderColor: 'black', fontWeight: "bold", fontFamily: "Arial" }}>
                    <CardContent>
                      <Typography sx={{ fontSize: 14, lineHeight: "-80px" }} color="text.secondary">
                        <img src={foto2} />
                      </Typography>
                      <Typography sx={{ fontSize: "16px", lineHeight: "50px", fontWeight: "bold", fontFamily: "Arial" }} >
                        Mobil Lengkap
                      </Typography>
                      <Typography sx={{ fontSize: "14px", lineHeight: "24px", fontWeight: "bold" }} >
                        Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat
                      </Typography>

                    </CardContent>

                  </Card>
                </Grid>

                <Grid item xs={3} sm={3} md={3}>
                  <Card sx={{ minWidth: 250, borderColor: 'black', fontWeight: "bold", fontFamily: "Arial" }}>
                    <CardContent>
                      <Typography sx={{ fontSize: 14, lineHeight: "-80px" }} color="text.secondary">
                        <img src={foto2} />
                      </Typography>
                      <Typography sx={{ fontSize: "16px", lineHeight: "50px", fontWeight: "bold", fontFamily: "Arial" }} >
                        Mobil Lengkap
                      </Typography>
                      <Typography sx={{ fontSize: "14px", lineHeight: "24px", fontWeight: "bold" }} >
                        Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat
                      </Typography>

                    </CardContent>

                  </Card>
                </Grid>

                <Grid item xs={3} sm={3} md={3}>
                  <Card sx={{ minWidth: 250, borderColor: 'black', fontWeight: "bold", fontFamily: "Arial" }}>
                    <CardContent>
                      <Typography sx={{ fontSize: 14, lineHeight: "-80px" }} color="text.secondary">
                        <img src={foto2} />
                      </Typography>
                      <Typography sx={{ fontSize: "16px", lineHeight: "50px", fontWeight: "bold", fontFamily: "Arial" }} >
                        Mobil Lengkap
                      </Typography>
                      <Typography sx={{ fontSize: "14px", lineHeight: "24px", fontWeight: "bold" }} >
                        Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat
                      </Typography>

                    </CardContent>

                  </Card>
                </Grid>

              </Grid>
            </Container>
          </Box>

          <Box sx={{
            marginTop: "50px",
          }}>
            <Stack direction="column">
              <Typography sx={{ fontSize: "24px", lineHeight: "75px", fontWeight: "bold", fontFamily: "Arial", textAlign: "center" }} >
                Testimonial
              </Typography>
              <Typography sx={{ fontSize: "14px", lineHeight: "20px", fontWeight: "bold", fontFamily: "Arial", textAlign: "center" }} >
                Berbagai review positif dari para pelanggan kami
              </Typography>

            </Stack>
          </Box>

          {/* SECTION 4 */}
          <Box sx={{
            marginTop: "50px",
          }}>


            <Carousel>

              {item_data.map((item) => (
                <Card sx={{ display: 'flex', borderColor: 'black' }} key={item}>
                  <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                    <CardContent sx={{ flex: '1 0 auto', m: 3 }}>
                      <Avatar alt="Remy Sharp" src={profilorang} />
                    </CardContent>
                  </Box>

                  <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                    <CardContent sx={{ flex: '1 0 auto' }}>
                      <Typography component="div" variant="h5">
                        <Rating name="half-rating" defaultValue={5} precision={0.5} />
                      </Typography>
                      <Typography variant="subtitle1" color="text.secondary" component="div" sx={{
                        fontWeight: "bold",
                      }}>
                        {item.description}
                      </Typography>
                      <Typography variant="subtitle1" color="text.secondary" component="div">
                        {item.name}
                      </Typography>
                    </CardContent>

                  </Box>

                </Card>

              ))}

            </Carousel>
          </Box>


          <Box m={3} p={6} sx={{
            marginTop: "50px",
            height: "auto",
            background: "#0D28A6",
            color: "#FFFFFF",




          }}>
            <Stack spacing={2} direction="column" sx={{
              textAlign: "center"
            }} >
              <Typography sx={{
                fontWeight: "bold",
                fontFamily: "Arial",
                fontSize: "36px"
              }}>
                Sewa Mobil di (Lokasimu) Sekarang
              </Typography>
              <Typography sx={{
                fontWeight: "bold",
                fontFamily: "Arial",
                fontSize: "14px"
              }}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
              </Typography>

              <Typography sx={{
                fontWeight: "bold",
                fontFamily: "Arial",
                fontSize: "14px"
              }}>

                <Button variant='contained' onClick={() => sewaMobil()} color='success' >Mulai Sewa Mobil</Button>
              </Typography>


            </Stack>

          </Box>


          <Box sx={{
            marginTop: "50px",
          }}>
            <Container>
              <Grid container spacing={2} columns={{ xs: 4, sm: 6, md: 12 }} >
                {/* KOLOM PERTAMA */}
                <Grid item xs={6} sm={6} md={6}>
                  <Typography sx={{
                    fontSize: "24px",
                    lineHeight: "45px",
                    fontFamily: "Arial",
                    fontWeight: "bold"
                  }}>
                    Frequently Asked Question
                  </Typography>
                  <Typography sx={{
                    fontSize: "14px",
                    lineHeight: "20px",
                    fontFamily: "Arial",
                    fontWeight: "bold"
                  }}>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit
                  </Typography>

                </Grid>
                {/* kolom ke 2 */}

                <Grid item xs={6} sm={6} md={6}>
                  {faq.map((itemfaq) => (

                    <FormControl fullWidth key={itemfaq.id}>
                      <InputLabel id="demo-simple-select-label">{itemfaq.faqnya}</InputLabel>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"

                        label="Age"
                        placeholder='Apa saja syarat yang dibutuhkan'


                      >

                        <MenuItem value={10}>test</MenuItem>
                      </Select>


                    </FormControl>




                  ))}


                  {/* <FormControl fullWidth>
                    <InputLabel id="demo-simple-select-label">{itemfaq.faqnya}</InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                     
                      label="Age"
                      placeholder='Apa saja syarat yang dibutuhkan'
                      key={itemfaq.id}
                      
                    >
                     
                      
                    </Select>
                    <br />
                    
                  </FormControl> */}




                </Grid>

              </Grid>
            </Container>

          </Box>

        </Container>
      </Box>


    </>
  )
}