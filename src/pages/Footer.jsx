import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import fb from '../assets/icon_facebook.png';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import ig from '../assets/icon_instagram.png';
import mail from '../assets/icon_mail.png';
import Stack from '@mui/material/Stack';
import Toolbar from '@mui/material/Toolbar';
import twitter from '../assets/icon_twitter.png';
import twitch from '../assets/icon_twitch.png';

import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';



function Copyright() {
  return (
    <Typography variant="body2" color="text.secondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://mui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}


export default function Footer() {
  return (
    <>
      <Box sx={{ bgcolor: 'background.paper', p: 6 }} component="footer">
        <Container maxWidth="lg">
          <Grid container spacing={6}  columns={{ xs: 3, sm: 3, md: 12 }} >
            <Grid item xs={2} md={3} sm={3}>
              <Typography sx={{
                fontSize: "14px",
                fontFamily: "Arial",
                lineHeight: "20px",
                fontWeight: "bold"
              }}>
                Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000
              </Typography>
              <Typography v sx={{
                fontSize: "14px",
                fontFamily: "Arial",
                lineHeight: "50px",
                fontWeight: "bold"
              }}>
                binarcarrental@gmail.com
              </Typography>
              <Typography gutterBottom variant='subtitle' sx={{
                fontSize: "14px",
                fontFamily: "Arial",
                lineHeight: "20px",
                fontWeight: "bold"
              }}>
                081-233-334-808
              </Typography>

            </Grid>
            <Grid item xs={2} md={3} sm={3} >
              <Typography gutterBottom variant='subtitle' sx={{
                fontSize: "14px",
                fontFamily: "Arial",
                lineHeight: "30px",

              }}>
                Our Service
              </Typography>
              <Typography gutterBottom sx={{
                fontSize: "14px",
                fontFamily: "Arial",
                lineHeight: "30px",

              }}>
                Why Us
              </Typography>
              <Typography gutterBottom sx={{
                fontSize: "14px",
                fontFamily: "Arial",
                lineHeight: "30px",

              }}>
                Testimonial
              </Typography>
              <Typography gutterBottom sx={{
                fontSize: "14px",
                fontFamily: "Arial",
                lineHeight: "30px",

              }}>
                FAQ
              </Typography>

            </Grid>
            <Grid item xs={2} md={3} sm={3} >
              <Typography gutterBottom sx={{
                fontSize: "14px",
                fontFamily: "Arial",
                lineHeight: "30px",
                fontWeight: "bold"

              }}>
                Connect With Us
              </Typography>
              <Stack direction="row" spacing={2}>
                <img src={fb}/>
                <img src={ig}/>
                <img src={twitter}/>
               
                <img src={mail}/>
                <img src={twitch}/>

              </Stack>

            </Grid>
            <Grid item xs={2} md={3} sm={3} >
              <Typography gutterBottom sx={{
                fontSize: "14px",
                fontFamily: "Arial",
                lineHeight: "30px",
                fontWeight: "bold"

              }}>
                Copyright Binar 20221
              </Typography>
              <Box sx={{
                width : "100px",
                height : "34px",
                background : "#0D28A6"
              }}>
                  
              </Box>
            </Grid>

          </Grid>
          

        </Container>

      </Box>
    </>
  )
}