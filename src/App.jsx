import { useState } from 'react';
import { Bolt } from '@mui/icons-material';
import CarList from './pages/CarList';
import { Link } from "react-router-dom";
import CarDetail from './pages/CarDetail';
import Header from './pages/Header';
import Home from './pages/Home';
import Notfound from './pages/Notfound';
import './pages/styles.css';

import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";


import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';

import gambar from './assets/img_car.png';





import Footer from './pages/Footer';
import CariMobil from './pages/CariMobil';
import DetailMobil from './pages/DetailMobil';
import PencarianData from './pages/PencarianData';




const navItems = ['Our Services', 'Why Us', 'Testimonial', 'FAQ'];
const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));


function App() {
  return (
    
   
    <BrowserRouter>
     <Header/>
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/detailmobil/:id' element={<DetailMobil/>} />
        <Route path='/carimobil' element={<CariMobil/>} />
        <Route path='/pencariandata' element={<PencarianData/>} />z
        <Route path='*' element={<Notfound />} />
      </Routes>
      <Footer/>
    </BrowserRouter>
    
  )
}

export default App;