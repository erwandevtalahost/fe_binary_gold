import { Container, Typography } from '@mui/material';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import FormControl from '@mui/material/FormControl';
import Grid from '@mui/material/Grid';
import { borderColor, Box } from '@mui/system';
import { useState, useEffect } from "react"
import { useNavigate } from "react-router-dom";

import InputLabel from '@mui/material/InputLabel';

import NativeSelect from '@mui/material/NativeSelect';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import NumberFormat from 'react-number-format';
import Stack from '@mui/material/Stack';
import Skeleton from '@mui/material/Skeleton';
import TextField from '@mui/material/TextField';






export default function PencarianData() {

    const [w, setW] = useState(window.innerHeight > 734 ? 50 : 70);
    const navigate = useNavigate()

    useEffect(() => {
        const handleResize = () => {
            setW(window.innerHeight > 734 ? 50 : 70);
        };

        window.addEventListener("resize", handleResize);

        return () => {
            window.removeEventListener("resize", handleResize);
        };
    }, []);

    //=---------
    


    function CariMobil() {
        navigate(`/carimobil`)
    }




    return (
        <>
            <main>
                <Box mt={w} sx={{ flexGrow: 1 }}>
                    <Container maxWidth="lg" sx={{
                        overflow: 'auto'
                    }}>
                        <Box
                            sx={{

                                maxwidth: "100%",
                                height: "100px",
                                padding: 3,
                                margin: 0,
                                // border: '1px solid black' ,
                                align: "center"

                            }}>

                            <Stack direction="row" spacing={2} sx={{
                                boxShadow: "12px"
                            }}>
                                
                                <TextField

                                    id="outlined-required"
                                    label="Nama Mobil"
                                    defaultValue="Ketik nama/tipe mobil"
                                    sx={{
                                        width: "800px"
                                    }}
                                />


                                <FormControl fullWidth>
                                    <InputLabel id="demo-simple-select-label">Kategori</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        label="Kategori"
                                        placeholder="Apa saja syarat yang dibutuhkan"

                                    >
                                        <MenuItem value={10}>Masukan Kapasitas Mobil</MenuItem>

                                    </Select>


                                </FormControl>

                                <FormControl fullWidth >
                                    <InputLabel id="demo-simple-select-label">Harga</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        label="Kategori"
                                        placeholder="Apa saja syarat yang dibutuhkan"
                                    >

                                        <MenuItem value={10}>Masukan Harga Sewa per Hari</MenuItem>
                                    </Select>


                                </FormControl>

                                <FormControl fullWidth>
                                    <InputLabel id="demo-simple-select-label">Status</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        label="Kategori"
                                        placeholder="Apa saja syarat yang dibutuhkan"
                                    >

                                        <MenuItem value={10}>Disewa</MenuItem>
                                    </Select>


                                </FormControl>
                                <Button variant='contained' color='success' onClick={() => CariMobil()} sx={{
                                    fontSize: "10px"
                                }}>Cari Mobil</Button>


                            </Stack>
                        </Box>



                    </Container>

                </Box>
            </main>
        </>
    )
}
