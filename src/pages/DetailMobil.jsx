import React from 'react'
import { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import Button from '@mui/material/Button';
import { borderColor, Box } from '@mui/system';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Container from '@mui/material/Container';

import FormControl from '@mui/material/FormControl';
import Grid from '@mui/material/Grid';
import GroupIcon from '@mui/icons-material/Group';
import InputLabel from '@mui/material/InputLabel';

import NumberFormat from 'react-number-format';
import Stack from '@mui/material/Stack';
import Select from '@mui/material/Select';
import TextField from '@mui/material/TextField';
import MenuItem from '@mui/material/MenuItem';
import { Typography } from '@mui/material';

const URL = "https://bootcamp-rent-car.herokuapp.com/admin/car";

export default function DetailMobil() {
    const [car, setCar] = useState('')
    const { id } = useParams();

    async function getCar() {
        try {
            const res = await window.fetch(`${URL}/${id}`);
            const data = await res.json();
            setCar(data)
        } catch (e) {
            console.log(e)
        }
    }
    useEffect(() => {
        getCar()
    }, [])

    //============= SET MARGIN HEADER
    const [w, setW] = useState(window.innerHeight > 734 ? 50 : 70);

    useEffect(() => {
        const handleResize = () => {
            setW(window.innerHeight > 734 ? 50 : 70);
        };

        window.addEventListener("resize", handleResize);

        return () => {
            window.removeEventListener("resize", handleResize);
        };
    }, []);


    //=======
    return (
        <>
            <Box mt={w}
                sx={{

                    maxwidth: "100%",
                    height: "100px",
                    padding: 3,
                    
                    // border: '1px solid black' ,
                    align: "center"

                }}>

               <Container maxWidth="lg">
                <Card sx={{ minWidth: 500, height : "150px" }}>
                <Stack m={5} direction="row" spacing={2} sx={{
                                boxShadow: "12px",
                                
                            }}>
                                
                                <TextField

                                    id="outlined-required"
                                    label="Nama Mobil"
                                    defaultValue="Ketik nama/tipe mobil"
                                    size="small"
                                    disabled
                                    sx={{
                                        width: "500px"
                                    }}
                                />


                                <FormControl sx={{
                                    m: 1,
                                    minWidth : 250
                                }} size="small" disabled>
                                    <InputLabel id="demo-simple-select-label">Kategori</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        label="Kategori"
                                        placeholder="Apa saja syarat yang dibutuhkan"

                                    >
                                        <MenuItem value={10}>Masukan Kapasitas Mobil</MenuItem>

                                    </Select>


                                </FormControl>

                                <FormControl sx={{
                                    m: 1,
                                    minWidth : 250
                                }} size="small" disabled >
                                    <InputLabel id="demo-simple-select-label">Harga</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        label="Kategori"
                                        placeholder="Apa saja syarat yang dibutuhkan"
                                    >

                                        <MenuItem value={10}>Masukan Harga Sewa per Hari</MenuItem>
                                    </Select>


                                </FormControl>

                                <FormControl sx={{
                                    m: 1,
                                    minWidth : 200
                                }} size="small" disabled>
                                    <InputLabel id="demo-simple-select-label 2">Status</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        label="Harga"
                                        placeholder="Apa saja syarat yang dibutuhkan"
                                    >

                                        <MenuItem value={10}>Disewa</MenuItem>
                                    </Select>


                                </FormControl>
                               


                            </Stack>
                            </Card>
                       </Container>     

            </Box>


            <Box m={2} sx={{ flexGrow: 1 }} p={3}>
                <Container maxWidth="lg" sx={{
                    overflow: 'auto',
                    py: 8
                }}>
                    <Grid container spacing={4}>
                        <Grid item xs={12} sm={6} md={7}>
                            <Card
                                sx={{ height: '100%', display: 'flex', flexDirection: 'column', p: 3 }}
                            >
                                

                                <Typography variant='h6' sx={{
                                    fontSize: "14px",
                                    fontFamily: "Arial",
                                    fontWeight: "bold",
                                    lineHeight: "10px"
                                }}>
                                    
                                    Tentang Paket
                                </Typography>

                                <Typography variant='h6' sx={{
                                    fontSize: "14px",
                                    fontFamily: "Arial",
                                    fontWeight: "bold",
                                    lineHeight: "50px"
                                }}>
                                    Include
                                </Typography>
                                <Typography sx={{
                                    fontSize: "14px",
                                    fontFamily: "Arial",
                                    fontWeight: "bold",
                                    lineHeight: "25px",
                                    color: "#8A8A8A"
                                }}>
                                    <ul>
                                        <li>Apa saja yang termasuk dalam paket misal durasi max 12</li>
                                        <li>Sudah termasuk bensin selama 12 jam</li>
                                        <li>Sudah termasuk Tiket Wisata</li>
                                        <li>Sudah termasuk pajak</li>
                                    </ul>
                                </Typography>

                                <Typography variant='h6' sx={{
                                    fontSize: "14px",
                                    fontFamily: "Arial",
                                    fontWeight: "bold",
                                    lineHeight: "50px"
                                }}>
                                    Exclude
                                </Typography>

                                <Typography sx={{
                                    fontSize: "14px",
                                    fontFamily: "Arial",
                                    fontWeight: "bold",
                                    lineHeight: "25px",
                                    color: "#8A8A8A"
                                }}>
                                    <ul>
                                        <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                        <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                                        <li>Tidak termasuk akomodasi penginapan</li>

                                    </ul>
                                </Typography>

                                <Typography variant='h6' sx={{
                                    fontSize: "14px",
                                    fontFamily: "Arial",
                                    fontWeight: "bold",
                                    lineHeight: "50px"
                                }}>
                                    Refund, Reschedule, Overtime
                                </Typography>


                                <Typography sx={{
                                    fontSize: "14px",
                                    fontFamily: "Arial",
                                    fontWeight: "bold",
                                    lineHeight: "25px",
                                    color: "#8A8A8A"
                                }}>
                                    <ul>
                                        <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                        <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp. 20.000/jam</li>
                                        <li>Tidak termasuk akomodasi penginapan</li>
                                        <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                        <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp. 20.000/jam</li>
                                        <li>Tidak termasuk akomodasi penginapan</li>
                                        <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                        <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp. 20.000/jam</li>

                                    </ul>
                                </Typography>

                            </Card>
                        </Grid>
                        <Grid item xs={12} sm={6} md={5}>
                            <Card
                                sx={{ height: '100%', display: 'flex', flexDirection: 'column', pl: 3 }}
                            >
                                <CardMedia component="img"
                                    sx={{
                                        // 16:9
                                        pt: '16.25%',
                                    }}
                                    image={car.image}
                                    alt="random" />
                                <Typography variant='h6' sx={{
                                    fontSize: "14px",
                                    fontFamily: "Arial",
                                    fontWeight: "bold",
                                    lineHeight: "50px"
                                }}>
                                    {car.name}
                                </Typography>
                                <Stack direction="row" spacing={1} >
                                    <Typography sx={{
                                        fontSize: "14px",
                                        fontFamily: "Arial",
                                        fontWeight: "bold",
                                        lineHeight: "13px"
                                    }}>
                                        <GroupIcon />
                                    </Typography>
                                    <Typography sx={{
                                        fontSize: "14px",
                                        fontFamily: "Arial",
                                        fontWeight: "bold",
                                        lineHeight: "13px",
                                        pt: 1
                                    }}>
                                        {car.category}
                                    </Typography>
                                </Stack>

                                <Stack direction="row" spacing={35} pt={5}>
                                    <Typography variant='h6' sx={{
                                        fontSize: "14px",
                                        fontFamily: "Arial",
                                        fontWeight: "bold",
                                        lineHeight: "50px"
                                    }}>
                                        Total
                                    </Typography>
                                    <Typography pr={3} sx={{
                                        fontSize: "14px",
                                        fontFamily: "Arial",
                                        fontWeight: "bold",
                                        lineHeight: "50px",
                                        textAlign: "right"
                                    }}>
                                        <NumberFormat
                                            value={car.price}
                                            className="foo"
                                            displayType={'text'}
                                            thousandSeparator={true}
                                            prefix={'Rp '}

                                        />
                                    </Typography>
                                    
                                    
                                </Stack>
                                <Typography variant='h6' sx={{
                                        fontSize: "14px",
                                        fontFamily: "Arial",
                                        fontWeight: "bold",
                                        lineHeight: "50px",
                                        mt: "120px"
                                    }}>
                                        <Link to='/pencariandata'>Kembali Ke Halaman Cari Mobil</Link>
                                    </Typography>
                            </Card>

                        </Grid>
                    </Grid>
                </Container>

            </Box>
        </>
    )
}
